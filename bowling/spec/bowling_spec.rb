require 'bowling'

RSpec.describe "Bowling game" do
  it 'sums 0 if user miss all' do
    expect(Bowling.new('---------------------').score).to eq(0)
  end

  it 'sums 1 if user hits one pin in one turn' do
    expect(Bowling.new('1--------------------').score).to eq(1)
  end

  it 'sums 1 for each one pin user hited' do
    expect(Bowling.new('1-1-1-1-1-1-1-1-1-1--').score).to eq(10)
  end

  it 'sums the number of pin hited in each turn' do
    expect(Bowling.new('27131218171-61417181-').score).to eq(63)
  end

  it 'have a spare inside the line' do
    expect(Bowling.new('2/131218171/61417181-').score).to eq(80)
  end

  it 'have a strike inside the line' do
    expect(Bowling.new('X-51257140X-72514380-').score).to eq(90)
  end

  xit 'all strikes' do
    expect(Bowling.new('XXXXXXXXXXXX').score).to eq(300)
  end  
end