class Bowling
  attr_accessor :score

  def initialize line
    @score = 0
    spare = 0
    strike = 0

    for i in (0..line.size).step(2)
      sum = 0
      if line[i + 1] == '/'
        sum += 10
        spare = 1
      elsif line[i] == 'X'
        sum += 10
        strike = 1
      else
        sum += line[i].to_i
        sum += line[i + 1].to_i
      end

      if spare == 1
        sum += line[i + 2].to_i
        spare = 0
      end

      if strike == 1
        sum += line[i + 2].to_i
        sum += line[i + 3].to_i
        strike = 0
      end

      @score += sum
    end
  end
end